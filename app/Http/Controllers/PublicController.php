<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function home(){
        return view('components.frontend.demo27');
    }

    public function product(){
        return view('components.frontend.demo27-product');
    }

    public function shop(){
        return view('components.frontend.demo27-shop');
    }

    public function cart(){
        return view('components.frontend.cart');
    }

    public function order(){
        return view('components.frontend.order');
    }

    public function checkout(){
        return view('components.frontend.checkout');
    }
    
}
