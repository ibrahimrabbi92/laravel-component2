<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(){
        return view('products.login');
    }

    public function store(Request $request) {
        return $request->all();
    }

    public function registration(){
        return view('products.registration');
    }
}
