<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function home() {
        return view('components.dashboard.index');
    }
    public function product() {
        return view('components.dashboard.products');
    }
}
