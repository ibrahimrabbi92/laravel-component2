<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/',[PublicController::class,'home']);

// Route::get('/', [DashboardController::class,'home']);

// Route::get('/products', [DashboardController::class,'product']);

// Route::get('/add-products', [DashboardController::class,'addproduct']);



Route::get('/demo27', [PublicController::class,'home']);


Route::get('/demo27-product', [PublicController::class,'product']);

Route::get('/demo27-shop', [PublicController::class,'shop']);

Route::get('/cart', [PublicController::class,'cart']);

Route::get('/order', [PublicController::class,'order']);

Route::get('/checkout', [PublicController::class,'checkout']);

